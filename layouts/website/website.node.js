module.exports = function ($) {
  $.head = $.templates['partials/head']()
  $.site_title = "Ryan Waters"
  $.year = new Date().getFullYear();
  $.css.push('https://fonts.googleapis.com/css?family=Lato|Lustria')
  $.css.push('https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js')
  $.css.push('https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.6/semantic.css')
  $.css.push('https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.6/semantic.min.js')
  $.layout('html5')
}
