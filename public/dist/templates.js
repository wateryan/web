module.exports = function (Handlebars) {
var template = Handlebars.template, templates = {};

templates['partials/head'] = template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "    <meta charset=\"UTF-8\" />\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\" />\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n";
},"useData":true});

templates['layouts/website'] = template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"ui secondary fixed menu\">\n  <!-- <a class=\"ui item\" href=\"home\">Home</a> -->\n</div>\n<main class=\"ui container\">\n  "
    + ((stack1 = ((helper = (helper = helpers.main || (depth0 != null ? depth0.main : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"main","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\n</main>\n<footer class=\"footer\">\n  <div class=\"ui center aligned grid container\">\n    <div class=\"column\">\n      &copy; "
    + alias4(((helper = (helper = helpers.year || (depth0 != null ? depth0.year : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"year","hash":{},"data":data}) : helper)))
    + " "
    + alias4(((helper = (helper = helpers.site_title || (depth0 != null ? depth0.site_title : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"site_title","hash":{},"data":data}) : helper)))
    + "\n    </div>\n  </div>\n</footer>\n";
},"useData":true});

templates['pages/home'] = template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"ui center aligned page vertically divided grid\">\n  <div class=\"single column bottom aligned row\">\n    <div class=\"column\">\n      <p>\n        Hello. I'm Ryan. I currently work at CME Group where I help create some of the applications backing the world's largest derivatives exchange.\n      </p>\n    </div>\n  </div>\n  <div class=\"three column center aligned row\">\n    <div class=\"column\">\n      <a class=\"item\" href=\"https://github.com/wateryan\" target=\"_blank\">\n        <i class=\"ui big github icon\"></i>\n        <div class \"middle aligned content\">\n         "
    + alias4(((helper = (helper = helpers.github || (depth0 != null ? depth0.github : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"github","hash":{},"data":data}) : helper)))
    + "\n        </div>\n      </a>\n    </div>\n    <div class=\"column\">\n      <a class=\"item\" href=\"https://www.linkedin.com/in/rmwaters\" target=\"_blank\">\n        <i class=\"big linkedin icon\"></i>\n        <div class \"middle aligned content\">\n         "
    + alias4(((helper = (helper = helpers.linkedIn || (depth0 != null ? depth0.linkedIn : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"linkedIn","hash":{},"data":data}) : helper)))
    + "\n       </div>\n      </a>\n    </div>\n    <div class=\"column\">\n      <a class=\"item\" href=\"https://gitlab.com/wateryan\" target=\"_blank\">\n        <i class=\"big gitlab icon\"></i>\n        <div class \"middle aligned content\">\n         "
    + alias4(((helper = (helper = helpers.gitlab || (depth0 != null ? depth0.gitlab : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"gitlab","hash":{},"data":data}) : helper)))
    + "\n       </di>\n      </a>\n    </div>\n  </div>\n</div>\n";
},"useData":true});

templates['layouts/html5'] = template({"1":function(container,depth0,helpers,partials,data) {
    return "    <link href=\""
    + container.escapeExpression(container.lambda(depth0, depth0))
    + "\" media=\"all\" rel=\"stylesheet\" type=\"text/css\" />\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "    <script type=\"text/javascript\" src=\""
    + container.escapeExpression(container.lambda(depth0, depth0))
    + "\"></script>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, options, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3="function", alias4=helpers.blockHelperMissing, buffer = 
  "<!doctype html>\n<html>\n  <head>\n    <title>"
    + container.escapeExpression(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data}) : helper)))
    + "</title>\n";
  stack1 = ((helper = (helper = helpers.css || (depth0 != null ? depth0.css : depth0)) != null ? helper : alias2),(options={"name":"css","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data}),(typeof helper === alias3 ? helper.call(alias1,options) : helper));
  if (!helpers.css) { stack1 = alias4.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  buffer += ((stack1 = ((helper = (helper = helpers.head || (depth0 != null ? depth0.head : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"head","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\n  </head>\n  <body>\n"
    + ((stack1 = ((helper = (helper = helpers.main || (depth0 != null ? depth0.main : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"main","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\n";
  stack1 = ((helper = (helper = helpers.js || (depth0 != null ? depth0.js : depth0)) != null ? helper : alias2),(options={"name":"js","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data}),(typeof helper === alias3 ? helper.call(alias1,options) : helper));
  if (!helpers.js) { stack1 = alias4.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  return buffer + "  </body>\n</html>";
},"useData":true});

return templates;
}