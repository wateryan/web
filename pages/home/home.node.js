module.exports = function ($) {
  $.title = "Home | Ryan Waters"
  $.github = "GitHub"
  $.linkedIn = "LinkedIn"
  $.gitlab = "GitLab"
  $.layout('website')
  $.render()
}
